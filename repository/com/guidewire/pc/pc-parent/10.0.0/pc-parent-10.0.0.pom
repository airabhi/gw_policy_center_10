<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.guidewire.pc</groupId>
  <artifactId>pc-parent</artifactId>
  <version>10.0.0</version>
  <packaging>pom</packaging>
  <properties>
    <product-model-codegen>com.guidewire.pl.productmodel:productmodel-codegen-gradle-executor:3.0.16</product-model-codegen>
    <entity-codegen>com.guidewire.pl.entity:entity-codegen-gradle-executor:10.6.0</entity-codegen>
    <localization-codegen>com.guidewire.pl.localization:localization-codegen-gradle-executor:10.0.0</localization-codegen>
    <perm-codegen>com.guidewire.pl.permission:permission-codegen-gradle-executor:10.0.2</perm-codegen>
    <pcf-codegen>com.guidewire.pl.pcfcodegen:pcfcodegen-gradle-executor:10.7.4</pcf-codegen>
    <xml-codegen>com.guidewire.pl.xml:gw-xml-gradle-executor:3.0.22</xml-codegen>
    <Bundle-DocURL>http://guidewire.com</Bundle-DocURL>
    <Bundle-License>nourl</Bundle-License>
    <Bundle-RequiredExecutionEnvironment>JavaSE-1.7</Bundle-RequiredExecutionEnvironment>
    <Bundle-Vendor>Guidewire Software, Inc.</Bundle-Vendor>
    <xmlCodegenMappedTypes>gw.lang.Blob,gw.api.financials.CurrencyAmount,gw.pl.currency.MonetaryAmount,gw.api.database.spatial.SpatialPoint,gw.api.database.spatial.SpatialPolygon,gw.xml.xsd.ArrayAction</xmlCodegenMappedTypes>
    <oracleJdbcDriver>gw.managed:ojdbc8:12.2.0.1</oracleJdbcDriver>
    <sqlServerJdbcDriver>com.microsoft.sqlserver:mssql-jdbc:6.2.1.jre8</sqlServerJdbcDriver>
    <postgreSqlJdbcDriver>org.postgresql:postgresql:42.2.1</postgreSqlJdbcDriver>
    <h2JdbcDriver>com.h2database:h2:1.2.147</h2JdbcDriver>
    <appCode>pc</appCode>
    <appName>PolicyCenter</appName>
    <port>8180</port>
  </properties>
  <parent>
    <groupId>com.guidewire.pc</groupId>
    <artifactId>pc-all</artifactId>
    <version>10.0.0</version>
    <relativePath>../../../../build/publications/mavenJava/pom-default.xml</relativePath>
  </parent>
  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-cmd-common</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-cmdline</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-content</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-custbuild-test</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-examples</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-gunit</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-gunit-content</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-perf</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-pl</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-pl-bizrules</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-pl-bizrules-content</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-pl-bizrules-test</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-pl-content</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-pl-heatmap</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-pl-personaldata</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-pl-personaldata-content</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-pl-solr</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-pl-test</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-plugin</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-run</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-solr</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-FR</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-JP</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-cc-integration-content</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-changesentities</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-hvq</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-i18n</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-multicurrency</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-personaldata</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-pmloader</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-rolling</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-solr</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-test-suite</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-tools</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-typeloader</artifactId>
        <version>10.0.0</version>
      </dependency>
      <dependency>
        <groupId>com.guidewire.pc</groupId>
        <artifactId>pc-verify</artifactId>
        <version>10.0.0</version>
      </dependency>
    </dependencies>
  </dependencyManagement>
  <dependencies/>
</project>
