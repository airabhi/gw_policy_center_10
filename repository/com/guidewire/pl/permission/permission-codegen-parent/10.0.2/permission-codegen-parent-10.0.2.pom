<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.guidewire.pl.permission</groupId>
  <artifactId>permission-codegen-parent</artifactId>
  <version>10.0.2</version>
  <packaging>pom</packaging>

  <name>Aggregation POM for all Permission Codegen projects</name>

  <scm>
    <developerConnection>scm:git:ssh://git@stash.guidewire.com/pl/permission-codegen.git</developerConnection>
    <connection>scm:git:https://stash.guidewire.com/scm/pl/permission-codegen.git</connection>
    <tag>HEAD</tag>
  </scm>

  <distributionManagement>
    <repository>
      <id>com.guidewire.pl.releases</id> <!-- id must match id in the ~/.m2/settings.xml! -->
      <url>https://nexus.guidewire.com/content/repositories/releases/</url>
      <name>Guidewire internal releases repository</name>
    </repository>
    <snapshotRepository>
      <!-- snapshots-dev does not require authentication so id need not match id in the ~/.m2/settings.xml! -->
      <id>com.guidewire.pl.snapshots-dev</id>
      <url>https://nexus.guidewire.com/content/repositories/snapshots-dev/</url>
      <name>Guidewire internal snapshots repository</name>
    </snapshotRepository>
  </distributionManagement>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>

  <build>
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>2.5.1</version>
          <configuration>
            <source>1.8</source>
            <target>1.8</target>
            <compilerArguments>
              <Xlint />
            </compilerArguments>
            <showWarnings>true</showWarnings>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>2.8.2</version>
          <configuration>
            <deployAtEnd>true</deployAtEnd>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>

  <modules>
    <module>permission-codegen</module>
    <module>permission-codegen-gradle-executor</module>
  </modules>

  <pluginRepositories>
    <pluginRepository>
      <id>gw.releases.build</id>
      <url>https://nexus.guidewire.com/content/repositories/releases-group/</url>
      <snapshots><enabled>false</enabled></snapshots>
    </pluginRepository>
    <pluginRepository>
      <id>gw.snapshots-group</id>
      <url>https://nexus.guidewire.com/content/repositories/snapshots-group/</url>
      <releases><enabled>false</enabled></releases>
    </pluginRepository>
  </pluginRepositories>

  <repositories>
    <repository>
      <id>gw.releases-group</id>
      <url>https://nexus.guidewire.com/content/repositories/releases-group/</url>
      <snapshots><enabled>false</enabled></snapshots>
    </repository>
    <repository>
      <id>gw.snapshots-group</id>
      <url>https://nexus.guidewire.com/content/repositories/snapshots-group/</url>
      <releases><enabled>false</enabled></releases>
    </repository>
  </repositories>
</project>
